from celery.app import Celery
from celery.utils.log import get_task_logger
from src.services.model import SentimentClassifier
import os

logger = get_task_logger(__name__)

backend_url = os.getenv("REDIS_URL", "redis://localhost:6379")
broker_url = os.getenv("RABBITMQ_URL", "amqp://guest:guest@localhost:5672")

celery_app = Celery(
    "celery_worker",
    broker=broker_url,
    backend=backend_url
)

@celery_app.task(name="text_analyzer")
def text_analyzer(input_text: str):
    logger.info(f"Analyzing text request: {input_text}")
    result = SentimentClassifier.predict_sentiment(input_text)
    logger.info(f"Result: {result}")
    return result
