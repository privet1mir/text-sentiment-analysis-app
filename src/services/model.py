from transformers import pipeline
import logging
import torch

torch.set_num_threads(1)

logger = logging.getLogger(__name__)

class SentimentClassifier:
    """
    Sentiment classifier based on pre-trained bert-model
    """
    pipe = pipeline("text-classification", model="MarieAngeA13/Sentiment-Analysis-BERT")

    @classmethod
    def predict_sentiment(cls, text:str) -> dict:
        """Predicts the sentiment based on input text

        Args:
            text (str): Input text for analysis

        Returns:
            dict: Result with the following structure: {'label': 'positive', 'score': 0.987019419670105}
        """
        result = cls.pipe(text)
    
        return result
    