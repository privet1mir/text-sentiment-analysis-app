# Text sentiment analysis app


## Intro

Web-aplication with Bert-like model on a backend for sentiment analysis. Project based on FastAPI. For requests processing celery worker was used with redis backend and rabbitmq broker. For front part gradio was used. Enjoy!


## Content

- [Project structure](#project-structure)

- [Flow of Data](#flow-of-data)

- [Getting Started](#getting-started)

- [Gradio](#gradio)

- [Manual approach](#manual-approach)


## Project structure

```
.
├── .docker
│   └── Dockerfile              # Dockerfile for installing dependencies using poetry
├── pyproject.toml              # File with dependencies
├── poetry.lock              	# Locking file
├── docker-compose.yml          # Docker compose file for multi-container app initialization
└── src
    ├── app.py                  # Main Applicaition with gradio front
    ├── api                     # API routes
    │   ├── __init__.py         # Initialization
    │   ├── routes              # Пакет с маршрутами API
    │   │   ├── __init__.py     # Initialization
    │   │   ├── healthcheck.py  # Route for app health checking
    │   │   ├── request.py      # Route for request processing
    │   │   ├── predict.py      # Route for predictions obtaining
    │   │   └── router.py       # Main router
    ├── schemas                 # Data models
    │   ├── __init__.py         # Initialization
    │   ├── celery_worker.py    # Celery app and tasks initialization 
    │   ├── healthcheck.py      # Model for app health checking
    │   └── text_requests.py    # Model for api-requests 
    └── services                # Business logic
        ├── __init__.py         # Initialization
        ├── model.py            # Main ML model initialization
        └── utils.py            # Some extra utils
```


## Flow of Data

	1.	User Requests:
	•	A user sends a POST request to /request/ with text data.
	•	FastAPI receives the request and passes the text to the text_analyzer via Celery.
	•	Celery queues the task in RabbitMQ and returns a task_id to the user.

	2.	Asynchronous Processing:
	•	Celery worker picks up the task from RabbitMQ.
	•	text_analyzer processes the text using Bert-based model for sentiment analysis.
	•	The result is stored in Redis.
    
	3.	Result Retrieval:
	•	The user sends a GET request to /predict/.
	•	FastAPI queries Celery (Redis) for the task result.
	•	FastAPI returns the status and result to the user.

## Getting Started 

``` docker-compose up --build ```

Web-server on:

```http://0.0.0.0:8000```

Swagger-ui on:

```http://0.0.0.0:8000/docs```

Flower - web-application for monitoring and managing Celery tasks on: 

```http://0.0.0.0:5555```

To test endpoints do the following: 

1. POST Request to Submit Text: 
```
curl -X 'POST' \
  'http://127.0.0.1:8000/api/request/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "text": "Hi lovely!"
}'
```

Response should be the following: 
```
{"task_id":"ffd735f1-584c-40ac-8056-bb5ef38f13c5"}
```

2. GET Request to see the result of sentiment analysis: 

```
# use your task id
curl -X 'GET' \
  'http://127.0.0.1:8000/api/predict/?task_id=ffd735f1-584c-40ac-8056-bb5ef38f13c5' \
  -H 'accept: application/json'
```

The response should be the following:

```
{"task_id":"ffd735f1-584c-40ac-8056-bb5ef38f13c5",
	"status":"SUCCESS",
		"result":[{"label":"positive","score":0.9009915590286255}]}
```

## Gradio 

You can reach gradio-based web-application on ```http://127.0.0.1:8000/gradio```

The interface will be the following: 

<img src="docs/gradio_app.png" alt="Alternative Text">

Your options are: 

* Input text for sentiment analysis

* If you want to see score (means confidence in prediction) you can toggle checkbox 

Also lazy caching was implemented. It means that each example will only get cached after it is first used.


## Manual approach

To start web-application manually in the project folder you can do the following (macOS): 

1. Start the RabbitMQ server: ```brew services start rabbitmq```

2. Start Reddis for backend: ```brew services start redis```

3. Start the celery worker (in poetry shell): ```celery -A src.schemas.celery_worker worker --loglevel=info```

4. Run the FastAPI (in poetry shell): ```uvicorn src.app:app --reload```
