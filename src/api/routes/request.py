import logging
from fastapi import APIRouter
from src.schemas.celery_worker import text_analyzer
from src.schemas.text_requests import TextRequest

logger = logging.getLogger(__name__)

router = APIRouter()

@router.post("/request/")
async def analyze_sentiment(input_request: TextRequest): 
    logger.info(f"Received text request: {input_request.text}")
    task = text_analyzer.delay(input_request.text)
    logger.info(f"Task ID: {task.id}, Task status: {task.status}")
    
    return {"task_id": task.id}
