import logging
from fastapi import APIRouter
from celery.result import AsyncResult

logger = logging.getLogger(__name__)

router = APIRouter()

@router.get("/predict/")
async def result(task_id: str):
    result = AsyncResult(task_id)
    logger.info(f"Task {task_id} status: {result.status}, result: {result.result}")

    return {"task_id": task_id, "status": result.status, "result": result.result}
