from pydantic import BaseModel

class TextRequest(BaseModel):
    """Texts request for ML-model sentiment analysis

    Args:
        text (str): Text for analysis
    """

    text: str

    class Config: 
        """Model example
        """

        schema_extra = {
            "example": {
                "text": "Hi lovely!"
            }
        }
