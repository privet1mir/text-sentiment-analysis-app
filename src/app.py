import logging
import httpx
import os
import asyncio
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.api.routes.router import router as api_router
import gradio as gr

logger = logging.getLogger(__name__)

def get_app() -> FastAPI: 

    app = FastAPI(
        title="Sentiment analysis service",
        version="0.1.0",
        description="Web service with ML-model backend for text input sentiment classifying"
    )
    app.add_middleware(
        CORSMiddleware,         
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["GET", "POST"],
        allow_headers=["*"],
    )
    app.include_router(api_router, prefix="/api")

    logger.info("FastAPI application has been initialized")

    return app

app = get_app()

"""
GRADIO PART
"""

fastapi_url = os.getenv("FASTAPI_URL", "http://127.0.0.1:8000")

async def submit_to_fastapi(text, show_score):
    try:
        async with httpx.AsyncClient() as client:
            payload = {"text": text}
            response = await client.post(f"{fastapi_url}/api/request/", json=payload)
            response.raise_for_status()
            task_id = response.json()["task_id"]

            await asyncio.sleep(2)  # wait for the task to complete

            get_url = f"{fastapi_url}/api/predict/?task_id={task_id}"
            final_response = await client.get(get_url)
            final_response.raise_for_status()
            result = final_response.json()["result"][0]
            label = result["label"]
            score = round(result["score"], 2) if show_score else None

            return label, score

    except httpx.HTTPStatusError as e:
        logger.error(f"HTTP error occurred: {e.response.status_code}")
        return "HTTP error occurred."
    except Exception as e:
        logger.error(f"An error occurred: {str(e)}")
        return "An error occurred."

gradio_interface = gr.Interface(theme=gr.themes.Soft(),
    fn=submit_to_fastapi,
    inputs=[
        gr.Textbox(label="Input text"),
        gr.Checkbox(label="Show score", value=True, interactive=True)
    ],
    outputs=[
        gr.Textbox(label="Sentiment"),
        gr.Textbox(label="Score")
        ],
    title="Sentiment analysis web-app with BERT-based model backend",
    description="Write the text input for processing",
    examples=[
            ["Love you", True],
            ["Hate you", True], 
            ["Greet you", False]
        ],
    cache_examples="lazy"
)

app = gr.mount_gradio_app(app, gradio_interface, path="/gradio")
